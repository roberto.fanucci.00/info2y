Si consideri l'architettura MIC-1. Si scriva un programma (IJVM Assembly Language) che ordini in maniera crescente (verso l’alto della pila) i 5 valori in cima alla pila.

Esempi:

(1,1,1,1,1) -> (1,1,1,1,1)
(78,2,2,2,2) -> (2,2,2,2,78)
(78,78,2,78,78) -> (2,78,78,78,78)
(31,54,31,14,14) -> (14,14,31,31,54)
(64,83,17,1,90) -> (1,17,64,83,90)
(64,90,1,17,83) -> (1,17,64,83,90)
(90,17,64,1,83) -> (1,17,64,83,90)

N.B. per la consegna: prevedere nella prima parte del programma consegnato (da nominare cognome_nome_matricola.jas) 5 istruzioni BIPUSH per il caricamento sulla pila dei 5 valori da ordinare