// POLIMI IOL
// PROVA IN ITINERE N. 1 - ARCHITETTURA DEI CALCOLATORI - AA 2016/2017 24nov16
// RUSSO LUDOVICO - matr. 811727 - n. persona 10473246
//
// Programma in IJVM Assembly Language che legge un elenco di valori numerici interi (non nulli) immessi dalla tastiera
//(con un max di 8 cifre HEX ciascuno) e visualizza a schermo:
// A) il minimo ed il massimo valore presente nell�elenco
// B) la media (valore intero) dei valori presenti nell�elenco
// C) quante volte il valore massimo ed il valore minimo sono presenti nell�elenco   
// Funzionamento: 
// 1) digitare i numeri esadecimali (lettere maiuscole per le cifre A, B, C, D, E, F), intervallati fra loro dal tasto invio;
// 2) per interrompere la sequenza di immissione dopo l'ultimo numero digitare "0" e premere invio;
// 3) i risultati saranno automaticamente stampati a schermo in formato HEX a 8 cifre.

.constant
OBJREF 0x0						// serve per l'istruzione INVOKEVIRTUAL
.end-constant

.main				 

.var							// variabili locali del main
numtot							// cardinalit� dell'insieme dei numeri inseriti
sommatot						// somma complessiva dei numeri inseriti
min								// valore minimo dei numeri inseriti
max								// valore massimo dei numeri inseriti
media							// media aritmetica dei numeri inseriti (valore intero)
n_min							// n. occorrenze del valore minimo
n_max							// n. occorrenze del valore massimo
counter							// contatore dei confronti effettuati per la determinazione di n_min e n_max
.end-var

//////////////////////////////////////////////////////////////////////////////////////////////////////////	
// INIZIALIZAZIONE VARIABILI DEL MAIN

		BIPUSH 0x0		
		DUP
		DUP
		DUP
		DUP
		DUP
		DUP
		DUP
		ISTORE numtot			
		ISTORE sommatot
		ISTORE min
		ISTORE max
		ISTORE n_min
		ISTORE n_max
		ISTORE media
		ISTORE counter
					
//////////////////////////////////////////////////////////////////////////////////////////////////////////	
// DATA ENTRY E CALCOLO DEI RISULTATI

main0:	LDC_W OBJREF			// serve per l'istruzione INVOKEVIRTUAL
		INVOKEVIRTUAL insnum	// inserisci da tastiera un numero in formato esadecimale  
		DUP						//
		BIPUSH 0x0				//
		IF_ICMPEQ main6			// se il numero inserito � 0 interrompi il data entry e vai a main6 per il calcolo dei risultati
		BIPUSH 0x0				//
		ILOAD numtot			//
		ISUB					//
		IFLT main1				// se il numero inserito non � il primo vai a main1 per il calcolo del minimo
		DUP						// altrimenti assegna a min e max il valore del primo numero inserito
		ISTORE min				// 
		DUP						//
		ISTORE max				//
		GOTO main5				// e vai direttamente all'aggiornamento di sommatot e numtot
main1:	DUP						//
		ILOAD min				//
		ISUB					//
		IFLT main3				//	se il numero inserito (diverso dal primo) � inferiore al minimo corrente vai a main3 per aggiornare il minimo
main2:	DUP						// 	
		ILOAD max				//
		SWAP					//
		ISUB					//
		IFLT main4				// se il numero inserito (diverso dal primo) � maggiore del massimo corrente vai a main4 per aggiornare il massimo
		GOTO main5				// altrimenti vai direttamente all'aggiornamento di sommatot e numtot
main3:	DUP						//
		ISTORE min				// aggiorna il minimo corrente assegnandogli il valore del numero inserito
		GOTO main2				// e vai a main2 per il calcolo del massimo
main4: 	DUP						//
		ISTORE max				// aggiorna il massimo corrente assegnandogli il valore del numero inserito
main5:	DUP						//
		ILOAD sommatot			//
		IADD					//
		ISTORE sommatot			// aggiorna la somma totale corrente aggiungendo il valore del numero inserito
		IINC numtot 0x1			// aggiorna la cardinalit� dell'insieme dei numeri inseriti incrementandola di 1
		GOTO main0				// vai a main0 per inserire un nuovo numero
main6:  POP						// elimina lo 0 in cima alla pila (adesso la pila conterr� tutti i numeri inseriti dal pi� recente al meno recente) 
		BIPUSH 0x0				//
		ILOAD numtot			//
		IF_ICMPEQ main13		// se non ho inserito alcun numero interrompi il programma
		LDC_W OBJREF			// altrimenti
		ILOAD numtot			//
		ILOAD sommatot			//
		INVOKEVIRTUAL average	// calcola la media aritmetica (valore intero)  
		ISTORE media			// e memorizzala in media
main7:	ILOAD counter			//
		ILOAD numtot			//
		IF_ICMPEQ main12		// se sono stati ultimati tutti i confronti per il calcolo di n_min e n_max vai a main12 per la stampa dei risultati
		DUP						//
		ILOAD min				//
		IF_ICMPEQ main10		// se il numero corrente in cima alla pila coincide con il minimo vai a main10 per aggiornare n_min
main8:	ILOAD max				//		
		IF_ICMPEQ main11		// se il numero corrente in cima alla pila coincide con il massimo vai a main11 per aggiornare n_max
main9:	IINC counter 0x1		// aggiorna il contatore dei confronti incrementandolo di 1
		GOTO main7				// e vai a main7 per un nuovo confronto
main10:	IINC n_min 0x1			// aggiorna n_min incrementandolo di 1
		GOTO main8				// vai a main8 per il calcolo di n_max
main11:	IINC n_max 0x1			// aggiorna n_max incrementandolo di 1
		GOTO main9				// vai a main9 per aggiornare il numero di confronti effettuati
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////	
// VISUALIZZAZIONE A SCHERMO DEI RISULTATI	
		
main12:	BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		BIPUSH 0x4E				// "N"
		BIPUSH 0x49				// "I"
		BIPUSH 0x4D				// "M"
		OUT						//
		OUT						//
		OUT						//
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		LDC_W OBJREF		    //
		ILOAD min		 		//
		INVOKEVIRTUAL visual    // visualizza il minimo in formato HEX a 8 cifre  
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		BIPUSH 0x58				// "X"
		BIPUSH 0x41				// "A"
		BIPUSH 0x4D				// "M"
		OUT						//
		OUT						//
		OUT						//
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		LDC_W OBJREF			//
		ILOAD max		 		//
		INVOKEVIRTUAL visual    // visualizza il massimo in formato HEX a 8 cifre 
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		BIPUSH 0x47				// "G"
		BIPUSH 0x56				// "V"
		BIPUSH 0x41				// "A"
		OUT						//
		OUT						//
		OUT						//
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		LDC_W OBJREF			//
		ILOAD media		 		//
		INVOKEVIRTUAL visual   	// visualizza la media aritmetica (valore intero) in formato HEX a 8 cifre   
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		BIPUSH 0x4E				// "N"
		BIPUSH 0x49				// "I"
		BIPUSH 0x4D				// "M"
		BIPUSH 0x4E				// "N"
		OUT						//
		OUT						//
		OUT						//
		OUT						//
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		LDC_W OBJREF			//	  
		ILOAD n_min		 		//	
		INVOKEVIRTUAL visual 	// visualizza n_min in formato HEX a 8 cifre    
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		BIPUSH 0x58				// "X"
		BIPUSH 0x41				// "A"
		BIPUSH 0x4D				// "M"
		BIPUSH 0x4E				// "N"
		OUT						//
		OUT						//
		OUT						//
		OUT						//
		BIPUSH 0xA				// nuova linea
		OUT						// stampa nuova linea
		LDC_W OBJREF			//  
		ILOAD n_max		 		//
		INVOKEVIRTUAL visual    // visualizza n_max in formato HEX a 8 cifre
	
main13:	HALT  					// termina programma (interruzione simulatore)

.end-main

//////////////////////////////////////////////////////////////////////////////////////////////////////////	
// METODO PER IL DATA ENTRY DEI NUMERI HEX A 8 CIFRE

.method insnum()				// metodo per l'inserimento di un numero intero in formato esadecimale da tastiera (max 8 cifre)

.var
numero							// numero inserito
count	 						// contatore del numero di cifre 
.end-var

		BIPUSH 0x0				// inizializzazione numero
		BIPUSH 0x0				// inizializzazione counter
        ISTORE numero			//
		ISTORE count			//
ins:	IN						// legge un carattere dalla tastiera
		DUP						// lo duplica
		BIPUSH 0xA				// nuova linea
		IF_ICMPEQ return		// se tasto "invio" vai a return per uscita dal metodo
		DUP						//
		BIPUSH 0x30				//  
		ISUB					//
		IFLT canc				// se la codifica ASCII del carattere � inferiore a quella del carattere "0" vai a canc per cancellarlo
	 	DUP						//
		BIPUSH 0x3A				// 
		ISUB					//
		IFLT ins1				// se la codifica ASCII del carattere � inferiore a quella del carattere ":"  (carattere fra "0" e "9") vai a ins1
		DUP						//
		BIPUSH 0x41				//  
		ISUB					//
		IFLT canc				// se la codifica ASCII del carattere � inferiore a quella del carattere "A"  vai a canc per cancellarlo
		DUP						//
		BIPUSH 0x46				//  
		SWAP					//  
		ISUB					//
		IFLT canc				//  se la codifica ASCII del carattere � superiore a quella del carattere "F"  vai a canc per cancellarlo
		DUP						// 	 
		OUT						//  stampa carattere valido fra "A" e "F"
		BIPUSH 0x37				//  
		ISUB					//	converti carattere in cifra
		GOTO shift				//   
ins1:  	DUP						//
		OUT						// 	stampa carattere valido fra "0" e "9"
		BIPUSH 0x30				//  converti carattere in cifra
		ISUB					//
shift:	ILOAD numero			// moltiplica ultimo numero memorizzato per 2^4 (sposta a sinistra di una posizione esadecimale)
		DUP						//
		IADD					//
		DUP						//
		IADD					//
		DUP						//
		IADD					//
		DUP						//
		IADD					//	
		IADD					// aggiungi nuova cifra al numero
		ISTORE numero			//
		IINC count 0x1			// aggiorno il contatore delle cifre inserite
		BIPUSH 0xA				// nuova linea
		ILOAD count				//
		BIPUSH 0x8				//
		IF_ICMPEQ return		// se ho gi� inserito 8 cifre esci dal metodo
		POP						//
		GOTO ins				// altrimenti inserisci nuova cifra
canc:	POP						// elimina carattere non valido
		GOTO ins 				// inserimento nuova cifra
return: OUT						// visualizza nuova linea
		ILOAD numero			// 
		IRETURN					// restituisci numero inserito

.end-method

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// METODO PER IL CALCOLO DELLA MEDIA ARITMETICA (VALORE INTERO)

.method average(numtot,sommatot) // metodo average per il calcolo della media aritmetica (valore intero)

.var
avg								// variabile usata per calcolare la media
.end-var
		
		BIPUSH 0x0				// inizializzo avg
		ISTORE avg				//
		ILOAD sommatot			// carico gli argomenti sulla pila
		ILOAD numtot  			//
		ISUB					//
		DUP 					//
		IFLT return  			// se la somma dei numeri � inferiore alla loro cardinalit� ritorno al main restituendo una media (valore intero) nulla
								// altrimenti simulo la divisione intera sottraendo ripetutamente dalla somma dei numeri la loro cardinalit�
avg0:	IINC avg 0x1 			// incremento avg (numero di sottrazioni effettuate = valore intero della media)
		ILOAD numtot   			//
		ISUB					//
		DUP 					//
		IFLT return  			// se la differenza progressiva � inferiore alla cardinalit� dei numeri restituisco al main il numero di sottrazioni effettuate 
		GOTO avg0				// altrimenti continuo a sottrarre
return: ILOAD avg   			// ritorno al main il valore di avg
		IRETURN					//

.end-method

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// METODO PER LA VISUALIZZAZIONE DEI RISULTATI IN FORMATO HEX A 8 CIFRE

.method visual(result) 			// metodo per la stampa a video di result in formato HEX a 8 cifre
				 	 
.var
currpos							// indice della cifra esadecimale corrente di result
pot2							// posizione, in termini di potenza del 2, della cifra binaria corrente di result
.end-var

vis: 	BIPUSH 0x9				// valore iniziale di currpos = 9	 
		ISTORE currpos			//
		BIPUSH 0x1				// valore iniziale di pot2 = 1	 
		ISTORE pot2				//
vis1:	BIPUSH 0x0				//
		ILOAD currpos			//
		BIPUSH 0x1				//
		ISUB					// 
		DUP						//
		IFEQ prt		 		// se currpos vale 1 vai a prt per avviare il ciclo di stampa
		ISTORE currpos			// altrimenti aggiorna currpos decrementato di 1
		ILOAD result			// pone in cima alla pila il numero da stampare
		ILOAD pot2		  		// 
		IAND					// maschera che restituisce la cifra binaria di result nella posizione corrente 	 
		IFEQ vis2		 		// se nella posizione corrente la cifra binaria � nulla vai a vis2
		BIPUSH 0x1		 		//	
		IADD					//	  
vis2: 	ILOAD pot2		 		//	
		DUP						//
		IADD					//
		ISTORE pot2 			// aggiorna pot2 scorrendo di un bit a sinistra
		ILOAD result			// pone in cima alla pila il numero da stampare
		ILOAD pot2				// 
		IAND			 		// maschera che restituisce la cifra binaria di result nella posizione corrente 
		IFEQ vis3		 		// se nella posizione corrente la cifra binaria � nulla vai a vis3
		BIPUSH 0x2		 		//
		IADD					//
vis3: 	ILOAD pot2		 		//
		DUP						//
		IADD					//
		ISTORE pot2				// aggiorna pot2 scorrendo di un bit a sinistra
		ILOAD result			// pone in cima alla pila il numero da stampare
		ILOAD pot2				// 
		IAND			 		// maschera che restituisce la cifra binaria di result nella posizione corrente 
		IFEQ vis4		        // se nella posizione corrente la cifra binaria � nulla vai a vis4
		BIPUSH 0x4		 		//
		IADD					//
vis4: 	ILOAD pot2		 		//       
		DUP						//
		IADD					//
		ISTORE pot2				// aggiorna pot2 scorrendo di un bit a sinistra
		ILOAD result			// pone in cima alla pila il numero da stampare
		ILOAD pot2				//
		IAND			        // maschera che restituisce la cifra binaria di result nella posizione corrente
		IFEQ vis5				// se nella posizione corrente la cifra binaria � nulla vai a vis5
		BIPUSH 0x8		 		//
		IADD					//
vis5:	ILOAD pot2		 		//
		DUP						//
		IADD					//
		ISTORE pot2				// aggiorna pot2 scorrendo di un bit a sinistra (nuova cifra esadecimale)
		GOTO vis1				// ricomincia
prt:  	POP			 			//
		POP						//
		BIPUSH 0x9				//
		ISTORE currpos			//
prt1:	ILOAD currpos		 	//
		BIPUSH 0x1				//
		ISUB					//
		DUP						//
		IFEQ return		 		// quando currpos vale 1 esci dal metodo (fine stampa)
		ISTORE currpos			//
		DUP						//
		BIPUSH 0xA		 		//
		ISUB                   	//
		IFLT prt2				// se la cifra � inferiore ad A vai a prt2 per la sua stampa
		BIPUSH 0x37		 		// altrimenti stampala qui
		IADD					//
		OUT			 			//
		GOTO prt1		 		//
prt2:	BIPUSH 0x30		 		//
		IADD					//
		OUT						//
		GOTO prt1		        //
return:	BIPUSH 0xA		 		// nuova linea
		OUT						//
		IRETURN			 		//

.end-method
